"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Routes = void 0;
const customersController_1 = require("../app/controllers/api/v1/customersController");
const adressController_1 = require("../app/controllers/api/v1/adressController");
const categoriesTypesController_1 = require("../app/controllers/api/v1/categoriesTypesController");
const CustomerCategoriesController_1 = require("../app/controllers/api/v1/CustomerCategoriesController");
const orderController_1 = require("../app/controllers/api/v1/orderController");
const OrderItemController_1 = require("../app/controllers/api/v1/OrderItemController");
const productCategoriesController_1 = require("../app/controllers/api/v1/productCategoriesController");
const productController_1 = require("../app/controllers/api/v1/productController");
const API_BASE_URL = '/api/v1';
var HTTP_METHODS;
(function (HTTP_METHODS) {
    HTTP_METHODS["Get"] = "get";
    HTTP_METHODS["Post"] = "post";
    HTTP_METHODS["Put"] = "put";
    HTTP_METHODS["Patch"] = "patch";
    HTTP_METHODS["Delete"] = "delete";
})(HTTP_METHODS || (HTTP_METHODS = {}));
function router(method, path, controller, action, baseUrl) {
    return {
        method,
        route: [!baseUrl ? API_BASE_URL : baseUrl, path].join('/'),
        controller,
        action
    };
}
exports.Routes = [
    router(HTTP_METHODS.Get, 'customers', customersController_1.CustomersController, 'index'),
    router(HTTP_METHODS.Get, 'customers/:id(\\d+)', customersController_1.CustomersController, 'show'),
    router(HTTP_METHODS.Post, 'customers', customersController_1.CustomersController, 'create'),
    router(HTTP_METHODS.Put, 'customers/:id(\\d+)', customersController_1.CustomersController, 'update'),
    router(HTTP_METHODS.Delete, 'customers/:id(\\d+)', customersController_1.CustomersController, 'destroy'),
    //////////////////////////////////////
    //      Customers Adress Crude      //
    //////////////////////////////////////
    router(HTTP_METHODS.Get, 'customers/adress', adressController_1.AdressController, 'index'),
    router(HTTP_METHODS.Get, 'customers/adress/:id(\\d+)', adressController_1.AdressController, 'show'),
    router(HTTP_METHODS.Post, 'customers/adress', adressController_1.AdressController, 'create'),
    router(HTTP_METHODS.Put, 'customers/adress/:id(\\d+)', adressController_1.AdressController, 'update'),
    router(HTTP_METHODS.Delete, 'customers/adress/:id(\\d+)', adressController_1.AdressController, 'destroy'),
    //////////////////////////////////////////////
    //         Categories Types Crude           //
    //////////////////////////////////////////////
    router(HTTP_METHODS.Get, 'categoriestypes', categoriesTypesController_1.CategoriesTypesController, 'index'),
    router(HTTP_METHODS.Get, 'categoriestypes/:id(\\d+)', categoriesTypesController_1.CategoriesTypesController, 'show'),
    router(HTTP_METHODS.Post, 'categoriestypes', categoriesTypesController_1.CategoriesTypesController, 'create'),
    router(HTTP_METHODS.Put, 'categoriestypes/:id(\\d+)', categoriesTypesController_1.CategoriesTypesController, 'update'),
    router(HTTP_METHODS.Delete, 'categoriestypes/:id(\\d+)', categoriesTypesController_1.CategoriesTypesController, 'destroy'),
    /////////////////////////////////////////////////
    //         CustomerCategoriesController        //
    /////////////////////////////////////////////////
    router(HTTP_METHODS.Get, 'customers/categories', CustomerCategoriesController_1.CustomerCategoriesController, 'index'),
    router(HTTP_METHODS.Get, 'customers/categories/:id(\\d+)', CustomerCategoriesController_1.CustomerCategoriesController, 'show'),
    router(HTTP_METHODS.Post, 'customers/categories', CustomerCategoriesController_1.CustomerCategoriesController, 'create'),
    router(HTTP_METHODS.Put, 'customers/categories/:id(\\d+)', CustomerCategoriesController_1.CustomerCategoriesController, 'update'),
    router(HTTP_METHODS.Delete, 'customers/categories/:id(\\d+)', CustomerCategoriesController_1.CustomerCategoriesController, 'destroy'),
    /////////////////////////////////////////////////
    //               OrderController               //
    /////////////////////////////////////////////////
    router(HTTP_METHODS.Get, 'customers/orders', OrderItemController_1.OrderItemController, 'index'),
    router(HTTP_METHODS.Get, 'customers/orders/:id(\\d+)', orderController_1.OrderController, 'show'),
    router(HTTP_METHODS.Post, 'customers/orders', orderController_1.OrderController, 'create'),
    router(HTTP_METHODS.Put, 'customers/orders/:id(\\d+)', orderController_1.OrderController, 'update'),
    router(HTTP_METHODS.Delete, 'customers/orders/:id(\\d+)', orderController_1.OrderController, 'destroy'),
    /////////////////////////////////////////////////
    //               OrderItemController           //
    /////////////////////////////////////////////////
    router(HTTP_METHODS.Get, 'customers/orders/items', OrderItemController_1.OrderItemController, 'index'),
    router(HTTP_METHODS.Get, 'customers/orders/items/:id(\\d+)', OrderItemController_1.OrderItemController, 'show'),
    router(HTTP_METHODS.Post, 'customers/orders/items', OrderItemController_1.OrderItemController, 'create'),
    router(HTTP_METHODS.Put, 'customers/orders/items:id(\\d+)', OrderItemController_1.OrderItemController, 'update'),
    router(HTTP_METHODS.Delete, 'customers/orders/items:id(\\d+)', OrderItemController_1.OrderItemController, 'destroy'),
    /////////////////////////////////////////////////
    //               ProductController             //
    /////////////////////////////////////////////////
    router(HTTP_METHODS.Get, 'products', productController_1.ProductController, 'index'),
    router(HTTP_METHODS.Get, 'products/:id(\\d+)', productController_1.ProductController, 'show'),
    router(HTTP_METHODS.Post, 'products', productController_1.ProductController, 'create'),
    router(HTTP_METHODS.Put, 'products/:id(\\d+)', productController_1.ProductController, 'update'),
    router(HTTP_METHODS.Delete, 'products/:id(\\d+)', productController_1.ProductController, 'destroy'),
    /////////////////////////////////////////////////
    //         ProductCategoriesController         //
    /////////////////////////////////////////////////
    router(HTTP_METHODS.Get, 'products/categories', productCategoriesController_1.ProductCategoriesController, 'index'),
    router(HTTP_METHODS.Get, 'products/categories/:id(\\d+)', productCategoriesController_1.ProductCategoriesController, 'show'),
    router(HTTP_METHODS.Post, 'products/categories', productCategoriesController_1.ProductCategoriesController, 'create'),
    router(HTTP_METHODS.Put, 'products/categories/:id(\\d+)', productCategoriesController_1.ProductCategoriesController, 'update'),
    router(HTTP_METHODS.Delete, 'products/categories/:id(\\d+)', productCategoriesController_1.ProductCategoriesController, 'destroy'),
];
