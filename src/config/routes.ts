// import { CustomersController } from '../app/controllers/api/v1/customersController'


const API_BASE_URL = '/api/v1'

enum HTTP_METHODS {
    Get = 'get',
    Post = 'post',
    Put = 'put',
    Patch = 'patch',
    Delete = 'delete'
}



function router(method: HTTP_METHODS, path: any, controller: any, action: any, baseUrl?: any) {
    return {
        method,
        route: [!baseUrl ? API_BASE_URL : baseUrl, path].join('/'),
        controller,
        action
    }
}

export const Routes = [
    
    //////////////////////////////////////
    //      Customers Adress Crude      //
    //////////////////////////////////////

    router(HTTP_METHODS.Get, 'customers', CustomersController, 'index'),
    router(HTTP_METHODS.Get, 'customers/:id(\\d+)', CustomersController, 'show'),
    router(HTTP_METHODS.Post, 'customers', CustomersController, 'create'),
    router(HTTP_METHODS.Put, 'customers/:id(\\d+)', CustomersController, 'update'),
    router(HTTP_METHODS.Delete, 'customers/:id(\\d+)', CustomersController, 'destroy'),
   ]