"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ApiController = exports.HTTP_STATUS_CODES = void 0;
var HTTP_STATUS_CODES;
(function (HTTP_STATUS_CODES) {
    HTTP_STATUS_CODES[HTTP_STATUS_CODES["Ok"] = 200] = "Ok";
    HTTP_STATUS_CODES[HTTP_STATUS_CODES["Created"] = 201] = "Created";
    HTTP_STATUS_CODES[HTTP_STATUS_CODES["NoContent"] = 204] = "NoContent";
    HTTP_STATUS_CODES[HTTP_STATUS_CODES["NotFound"] = 404] = "NotFound";
    HTTP_STATUS_CODES[HTTP_STATUS_CODES["UnprocessableEntity"] = 422] = "UnprocessableEntity";
    HTTP_STATUS_CODES[HTTP_STATUS_CODES["InternalServerError"] = 500] = "InternalServerError";
})(HTTP_STATUS_CODES = exports.HTTP_STATUS_CODES || (exports.HTTP_STATUS_CODES = {}));
class ApiController {
    permittedParams(raw, allowed) {
        return Object.entries(raw).reduce((acc, [key, value]) => {
            if (allowed.includes(key))
                acc[key] = value;
            return acc;
        }, {});
    }
}
exports.ApiController = ApiController;
