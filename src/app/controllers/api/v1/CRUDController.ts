import { Request, response, Response } from 'express'
import { ApiController, HTTP_STATUS_CODES } from './apiController'
import * as Yup from 'yup';

export class CRUDController extends ApiController {

    public static async index(req: Request, res: Response, model: any, include?: object, where?: object) {
        const colection = await model.findAll(include)

        if (!colection) {
            return "Nothing Here Yet"
        }
        return colection
    }

    public static async show(req: Request, res: Response, model: any, include?: object) {

        try {
            const resSchema = Yup.object().shape({
                "id": Yup.number().required('id is required for show'),
            })
            await resSchema.validate(req.params)

            const item = model.findById(req.params.id, include)

            if (!item) {
                return "Item not found"
            }

            return item
        } catch (error) {
            return error.message
        }

    }

    public create(req: Request, res: Response, model: any, Params: any) {

        const customerCategories = model.create(Params)

        res.status(HTTP_STATUS_CODES.Created)

        return customerCategories
    }

    public async update(req: Request, res: Response, model: any, Params: any) {

        const item = await model.findById(req.params.id)
        if (!item) {
            return "Item not found"
        }
        return item.update(Params)
    }

    public async destroy(req: Request, res: Response, model: any) {
        try {
            const item = await model.findById(req.params.id)
            if (!item) {
                return "Item not found"
            }
            const resSchema = Yup.object().shape({
                "id": Yup.number().required('id is required for Destroy'),
            })
            await resSchema.validate(req.params)

            await item.destroy()

            res.status(HTTP_STATUS_CODES.NoContent).end()
        } catch (error) {
            return error.message
        }
    }

    /////////////////////
    // Private methods //
    /////////////////////

}