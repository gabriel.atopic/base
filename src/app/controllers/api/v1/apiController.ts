export enum HTTP_STATUS_CODES {
    Ok = 200,
    Created = 201,
    NoContent = 204,
    NotFound = 404,
    UnprocessableEntity = 422,
    InternalServerError = 500,
}

export class ApiController {
    protected permittedParams(
        raw: Record<string, any>,
        allowed: string[]
      ): Record<string, any> {
        return Object.entries(raw).reduce((acc: Record<string, any>, [key, value]) => {
          if (allowed.includes(key)) acc[key] = value
    
          return acc
        }, {})
    }
}