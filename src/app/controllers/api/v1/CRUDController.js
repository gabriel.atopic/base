"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CRUDController = void 0;
const apiController_1 = require("./apiController");
const Yup = require("yup");
class CRUDController extends apiController_1.ApiController {
    static index(req, res, model, include, where) {
        return __awaiter(this, void 0, void 0, function* () {
            const colection = yield model.findAll(include);
            if (!colection) {
                return "Nothing Here Yet";
            }
            return colection;
        });
    }
    static show(req, res, model, include) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const resSchema = Yup.object().shape({
                    "id": Yup.number().required('id is required for show'),
                });
                yield resSchema.validate(req.params);
                const item = model.findById(req.params.id, include);
                if (!item) {
                    return "Item not found";
                }
                return item;
            }
            catch (error) {
                return error.message;
            }
        });
    }
    create(req, res, model, Params) {
        const customerCategories = model.create(Params);
        res.status(apiController_1.HTTP_STATUS_CODES.Created);
        return customerCategories;
    }
    update(req, res, model, Params) {
        return __awaiter(this, void 0, void 0, function* () {
            const item = yield model.findById(req.params.id);
            if (!item) {
                return "Item not found";
            }
            return item.update(Params);
        });
    }
    destroy(req, res, model) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const item = yield model.findById(req.params.id);
                if (!item) {
                    return "Item not found";
                }
                const resSchema = Yup.object().shape({
                    "id": Yup.number().required('id is required for Destroy'),
                });
                yield resSchema.validate(req.params);
                yield item.destroy();
                res.status(apiController_1.HTTP_STATUS_CODES.NoContent).end();
            }
            catch (error) {
                return error.message;
            }
        });
    }
}
exports.CRUDController = CRUDController;
