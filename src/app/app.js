"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const cors = require("cors");
const routes_1 = require("../config/routes");
const http = require("http");
const express = require('express');
class App {
    constructor() {
        this.app = express();
        this.server = new http.Server(this.app);
        this.middlewares();
        this.routes();
    }
    middlewares() {
        this.app.use(cors());
        this.app.use(express.json());
    }
    routes() {
        routes_1.Routes.forEach((route) => {
            ;
            this.app[route.method](route.route, (req, res, next) => {
                const result = new route.controller()[route.action](req, res, next);
                if (result instanceof Promise) {
                    result.then(result => result !== null && result !== undefined
                        ? res.json(result)
                        : undefined);
                }
                else if (result !== null && result !== undefined) {
                    res.json(result);
                }
            });
        });
    }
}
exports.default = new App().server;
