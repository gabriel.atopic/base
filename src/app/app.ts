import { Request, Response, NextFunction } from 'express'
import * as cors from 'cors'
import { Routes } from '../config/routes'
import * as http from 'http'

const express = require('express')

class App {
    app: any
    server: any

    constructor() {
        this.app = express()
        this.server = new http.Server(this.app)

        this.middlewares()
        this.routes()
    }

    middlewares() {
        this.app.use(cors())
        this.app.use(express.json())
    }

    routes() {
        Routes.forEach((route: any) => {
            ;(this.app as any)[route.method](
                route.route,
                (req: Request, res: Response, next: NextFunction) => {
                    const result = new (route.controller as any)()[route.action](
                        req,
                        res,
                        next
                    )
                    if (result instanceof Promise) {
                        result.then(result => 
                            result !== null && result !== undefined
                            ? res.json(result)
                            : undefined
                            )
                    } else if (result !== null && result !== undefined) {
                        res.json(result)
                    }
                }
            )
        })
    }
}

export default new App().server