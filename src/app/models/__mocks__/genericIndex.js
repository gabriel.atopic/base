const SequelizeMock = require('sequelize-mock');
const dbMock = new SequelizeMock();
 
function genericIndex(table, modelValues) {
    const model = dbMock.define(table, modelValues);
     
    model.$queryInterface.$useHandler((query, queryOptions, done) => {
        
    
        if(query === 'findAll'){
            return [modelValues]
        } else if(query === 'findById'){
            return model.build(modelValues);
        } else if(query === 'create'){
            return modelValues
        } else if(query === 'update'){
            return modelValues
        } else if(query === 'destroy'){
            return modelValues
        }
    })
    return model
}
 
module.exports = genericIndex